Meu nome é Kevin, o cachorro da família Olenscki. Neste site você encontrará tudo que precisa sobre mim, incluindo:

- Minha história:
    Como eu fui escolhido para minha familia e o que aconteceu comigo antes disso.

- Minhas conquistas:
    Me orgulho de poder dizer que sou um dos maiores caçadores de todos os tempos, caçando desde os mais ifames ratos até o mais malvado dos pisos.

- Minhas comidas favoritas:
    Meu gosto culinário é digno de sua atenção, as vezes, é importante ensinar alguns leigos como você a arte da gastronomia.

Aproveite :)

---
title: "Ratos caçados"
date: 2022-09-24T17:04:34-03:00
draft: false
layout: ratos
---

Manter a casa segura é uma das minhas maiores obrigações! Nenhuma invasão ao meu terreno é permitida, sejam passarinhos que só queriam fazer ninhos no jardim ou os ratos do esgoto.
---
title: "Pisos destruídos"
date: 2022-09-24T17:04:44-03:00
draft: false
layout: pisos
---

Um dos meus passatempos favoritos da casa é ajudar a arrumar os pisos do quintal. Ser prestativo é uma das formas de mostrar gratidão aos meus donos.
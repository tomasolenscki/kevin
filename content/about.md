---
title: "Minha história"
date: 2022-09-24T17:01:55-03:00
draft: false
layout: about
---

Fui adotado já com 6 meses de idade dia 20 de abril de 2016, quando minha primeira dona Maria Sofia me escolheu na feira de adoções. Dizem que eu fui resgatado de uma enchente e até hoje quando chegam com água perto saio correndo para o mais longe possível, se meus donos soubessem disso antes provavelmente me chamaria Cascão.
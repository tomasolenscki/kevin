---
title: "Melhores Comidas"
date: 2022-09-24T17:01:55-03:00
draft: false
layout: comidas
---

Para aqueles que começaram agora no mundo gastronômico, vou resumir rapidamente aqui com uma lista de top 10 comidas do mundo:

1. Banana
2. Banana
3. Banana verde
4. Banana até estragada
5. Maçã com banana
6. Banana misturada na ração
7. Banana com remédio escondido dentro
8. Petisco
9. Batido de Banana
10. Casca de banana

Essa lista é extremamente confiável e deveria substituir sua opinião se você pensa o contrário.
---
title: "Meu currículo"
date: 2022-09-24T17:01:55-03:00
draft: false
---

Caso esteja procurando um cão profíssional para sua residência, aqui estão minhas qualificações. Apesar de que eu não aceitaria sua proposta, amo muito minha família ... (A partir de 2 bananas podemos conversar).

| tarefa                     	| Sei fazê-la? 	|
|----------------------------	|--------------	|
| Limpar banheiro            	| Não          	|
| Limpar quarto              	| Não          	|
| Limpar Cozinha             	| Não          	|
| Tirar o lixo               	| Não          	|
| Lavar louça"               	| Não          	|
| Lavar roupa                	| Não          	|
| Cozinhar                   	| Não          	|
| Dar comida para o cachorro 	| Não          	|
| Patrulhar a casa           	| Sim          	|

Apesar de ser qualificado de mais, ainda dou uma chance para a casa Olenscki.